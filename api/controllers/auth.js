const jwt = require("../services/jwt");
const moment = require("moment");
const User = require("../models/user");

function willExpiredToken(token) {
    const { exp } = jwt.decodedToken(token);
    const currentDate = moment().unix().toFixed

    if (currentDate > exp) {
        return true;
    }
    return false;
}

function refreshAccessToken(req, res) {
    const { refreshToken } = req.body;
    const isTokenExpired = willExpiredToken(refreshToken);

    if (isTokenExpired) {
        res.status(401).send({ mesage: "El refresh Token ha expirado" });
    } else {
        const { id } = jwt.decodedToken(refreshToken);
        User.findOne({ _id: id }, (err, userStored) => {
            if (err) {
                res.status(500).send({ mesage: "Error de servidor" });
            } else {
                if (!userStored) {
                    res.status(401).send({ mesage: "Usuario no encontrado" });
                } else {
                    res.status(200).send({
                        accessToken: jwt.createAccessToken(userStored),
                        refreshToken: refreshToken
                    });
                }
            }
        });
    }
}

module.exports = {
    refreshAccessToken
}