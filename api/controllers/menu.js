const Menu = require("../models/menu");


function addMenu(req, res) {

    const { title, url, order, active } = req.body;
    const menu = new Menu();

    menu.title = title;
    menu.url = url;
    menu.order = order;
    menu.active = active;

    menu.save((err, createMenu) => {
        if (err) {
            res.status(500).send({ message: "Error Creando Menu. Verifique no exista" });
        } else {
            if (!createMenu) {
                res.status(500).send({ message: "Error creando Menu" });
            } else {
                res.status(200).send({ Menu: createMenu });
            }
        }
    })
}

getMenu = async (req, res) => {
    let menu = await Menu.find().sort({ order: "asc" }).exec((err, menuStored) => {
        if (err) {
            res.status(500).send({ message: "Error de servidor" });
        } else {
            if (!menuStored) {
                res.status(404).send({ message: "No se han encontrado menus" });
            } else {
                res.status(200).send({ menu: menuStored });
            }
        }
    });



}

module.exports = {
    addMenu,
    getMenu
}