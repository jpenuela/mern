const fetch = require("node-fetch")

const fs = require("fs");
const path = require("path");

const bcrypt = require('bcrypt-nodejs');
const jwt = require("../services/jwt");
const Tienda = require("../models/tienda");
const { exists } = require("../models/tienda");



getTiendas = async (req, res) => {
    let Tiendas = await Tienda.find();

    if (!Tiendas) {
        res.status(404).send({ message: "No se han encontrado usuarios" });
    } else {
        res.status(200).send({ Tiendas });
    }

}

function getTiendasActive(req, res) {
    const query = req.query;
    // console.log(req);

    Tienda.find({ active: query.active }).then((Tiendas) => {
        // console.log(Tiendas.length);
        if (!Tiendas) {
            res.status(404).send({ message: "No se han encontrado usuarios" });
        } else {
            res.status(200).send({ Tiendas });
        }
    });
}

updateTienda = async (req, res) => {
    const TiendaData = req.body;
    TiendaData.email = req.body.email.toLowerCase();
    const params = req.params;

    if (TiendaData.password) {
        await bcrypt.hash(TiendaData.password, null, null, (err, hash) => {
            if (err) {
                res.status(500).send({ message: "Error al encriptar la contraseña" });
            } else {
                TiendaData.password = hash;
            }
        })
    }

    try {
        let Tienda = await Tienda.findByIdAndUpdate({ _id: params.id }, TiendaData);
        if (!Tienda) {
            res.status(404).send({ message: "No se ha encontrado el usuario" });
        } else {
            res.status(200).send({ message: "Usuario Actualizado" });
        }
    } catch (error) {
        res.status(500).send({ message: "Ha ocurrido un error en el servidor" });
    }


    // console.log(TiendaData);
}

activateTienda = async (req, res) => {
    const { id } = req.params;
    const { active } = req.body;

    try {
        Tienda.findByIdAndUpdate(id, { active }, (err, TiendaStored) => {


            if (err) {
                res.status(500).send({ message: "Error al de servidor" });
            } else {
                if (!TiendaStored) {
                    res.status(404).send({ message: "El usuario no existe" });
                } else {
                    if (active) {
                        res.status(200).send({ message: "Usuario Activado Correctamente" });
                    } else {
                        res.status(200).send({ message: "Usuario Desactivado Correctamente" });
                    }

                }
            }
        });
    } catch (error) {
        res.status(500).send({ message: "Error al de servidor" });
    }



}

deleteTienda = async (req, res) => {
    const { id } = req.params;

    Tienda.findByIdAndRemove(id, (err, TiendaDeleted) => {
        if (err) {
            res.status(500).send({ message: "Error del servidor" });
        } else {
            if (!TiendaDeleted) {
                res.status(404).send({ message: "Usuario no existe" });
            } else {
                res.status(200).send({ message: "Usuario Eliminado" });
            }
        }
    })
}

newTienda = async (req, res) => {

    const tienda = new Tienda();

    const { cdtienda, codigohomologado, estado } = req.body
    tienda.cdtienda = cdtienda;
    tienda.codigohomologado = codigohomologado;
    tienda.estado = estado

    if (!cdtienda || !codigohomologado) {
        res.status(500).send({ message: "El codigo tienda y el codigo Homologado son requeridos" });
    } else {
        tienda.save((err, TiendaStored) => {
            if (err) {
                res.status(500).send({ message: "Error Creando Tienda. Verifique no exista" });
            } else {
                if (!TiendaStored) {
                    res.status(500).send({ message: "Error creando Tienda" });
                } else {
                    res.status(200).send({ Tienda: TiendaStored });
                }
            }
        })
    }


}

tiendaService = async (req, res) => {

    let token = await authCerverus();
    let body = await req.body;
    // console.log(body);
    await Tienda.findOne({ cdtienda: body.codigolocal }, (err, TienaStored) => {
        // console.log(TienaStored);
        if (err) {
            res.status(500).send({ message: err });
        } else {
            if (body.codigolocal.length !== 4) {
                res.status(403).send({ message: "El codigo de tienda debe tener 4 digitos" });
            } else {
                if (!TienaStored && (body.codigolocal.length == 3 || body.codigolocal.length == 2)) {
                    res.status(404).send({ message: "Tienda No Existe" });
                } else if (!TienaStored && body.codigolocal.length == 4) {
                    // res.status(200).send(req.body);
                } else {
                    body.codigolocal = TienaStored.codigohomologado;
                    body.codigoPOS = TienaStored.codigohomologado;
                }

                const url = 'https://serprualianza1.gco.com.co:8443/IntegracionPosWEB/rest/primeraCompra/validar';

                const params = {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "gsec-user-token": token.token
                    },
                    body: JSON.stringify(body)
                }

                return fetch(url, params)
                    .then((response) => {
                        return response.json();
                    })
                    .then((result) => {
                        res.status(200).send({ message: result });
                    })
                    .catch((err) => {
                        res.status(500).send({ message: err });
                    });
            }

        }
    })

}

authCerverus = async () => {


    const url = 'https://serprualianza1.gco.com.co:8443/SecurityServicesWeb/rest/security/authenticateUser';
    const body = JSON.stringify({
        request: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzQ1NiIsInBhc3N3b3JkIjoiYWRtaW40MzIifQ.JDms75Db878BuZ8sT-GZ2sQPKUZhAtKKxf4j8diFB7k"
    })
    const params = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Content-Length": body.length
        },
        body: body
    }

    return fetch(url, params)
        .then((response) => {
            return response.json();
        }).catch((err) => {
            return err;
        });
}

peticionSitar = async (token, body) => {

    const url = 'https://serprualianza1.gco.com.co:8443/IntegracionPosWEB/rest/primeraCompra/validar';

    const params = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "gsec-user-token": token
        },
        body: JSON.stringify(body)
    }
    await fetch(url, params).then(response => {
        if (response.status === 403) {
            let data = { status: 401, message: "No tiene permisos de acceso al recurso." };
            return data;
        }
    }).then(resp => {
        console.log(resp);
    });

}

module.exports = {
    getTiendas,
    getTiendasActive,
    updateTienda,
    activateTienda,
    deleteTienda,
    newTienda,
    tiendaService
}
