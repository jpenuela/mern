const fs = require("fs");
const path = require("path")
const bcrypt = require('bcrypt-nodejs');
const jwt = require("../services/jwt");
const User = require("../models/user");
const { exists } = require("../models/user");

function signUp(req, res) {
    const user = new User();

    const { name, lastname, email, password, repeatPassword } = req.body;
    user.name = name;
    user.lastname = lastname;
    user.email = email.toLowerCase();
    user.role = "admin";
    user.active = 0;

    if (!password || !repeatPassword) {
        res.status(404).send({ message: "Las contraseñas son obligatorias" });
    } else {
        if (password !== repeatPassword) {
            res.status(404).send({ message: "Las contraseñas deben ser iguales" });
        } else {
            bcrypt.hash(password, null, null, function (err, has) {
                if (err) {
                    res.status(500).send({ message: "Error al enciptar la contraseña" });
                } else {
                    user.password = has;

                    user.save((err, userStored) => {
                        if (err) {
                            res.status(500).send({ message: "Error de servidor", err: err });
                        } else {
                            if (!userStored) {
                                res.status(404).send({ message: "Error al crear el usuario", err: err });
                            } else {
                                res.status(200).send({ user: userStored });
                            }
                        }
                    });
                }
            });
        }
    }
}

function signIn(req, res) {

    const params = req.body;
    const email = params.email.toLowerCase();
    const password = params.password;

    User.findOne({ email }, (err, userStored) => {
        // console.log(userStored);
        if (err) {
            res.status(500).send({ message: "Error de Servidor User Stored" });
        } else {
            if (!userStored) {
                res.status(401).send({ message: "Usuario no encontrado" });
            } else {
                debugger;
                bcrypt.compare(password, userStored.password, (err, check) => {
                    if (err) {
                        res.status(500).send({ message: "Error de servidor Pass" });
                    } else if (!check) {
                        res.status(401).send({ message: "Usuario o contraseña invalida" });
                    }
                    else {
                        if (!userStored.active) {
                            // console.log(userStored.active);
                            res.status(200).send({ code: 200, message: "El usuario no se ha activado" })
                        } else {
                            res.status(200).send({
                                accessToken: jwt.createAccessToken(userStored),
                                refreshToken: jwt.createRefreshToken(userStored)
                            });
                        }
                    }
                });
            }
        }
    })

}

getUsers = async (req, res) => {
    let users = await User.find();

    if (!users) {
        res.status(404).send({ message: "No se han encontrado usuarios" });
    } else {
        res.status(200).send({ users });
    }

}

function getUsersActive(req, res) {
    const query = req.query;
    // console.log(req);

    User.find({ active: query.active }).then((users) => {
        // console.log(users.length);
        if (!users) {
            res.status(404).send({ message: "No se han encontrado usuarios" });
        } else {
            res.status(200).send({ users });
        }
    });
}

uploadAvatar = async (req, res) => {

    const params = req.params;

    // try {
    let userData = await User.findById({ _id: params.id });
    // console.log(userData);
    if (!userData) {
        res.status(404).send({ message: "No se ha encontrado el usuarior" });
    } else {
        let user = userData;

        if (req.files) {
            console.log(req.body.avatar);
            let filePath = req.body.avatar;
            // let fileSplit = filePath.split("\\");
            // // let fileName = fileSplit[2];
            let fileName = filePath.replace(/^.*[\\\/]/, "");
            let extSplit = fileName.split(".");
            let fileExt = extSplit[1];
            if (fileExt !== "png" && fileExt !== "jpg") {
                res.status(401).send({ message: "Tipo de imagen no permitida" });
            } else {
                user.avatar = fileName;
                User.findByIdAndUpdate({ _id: params.id }, user, (err, userResult) => {
                    if (err) {
                        res.status(500).send({ message: "Error de servidor" });
                    } else {
                        if (!userResult) {
                            res.status(404).send({ message: "No se ha encontrado ningun usuario" });
                        } else {
                            res.status(200).send({ avatarName: fileName });
                        }
                    }
                });
            }

        }
    }

    // } catch (error) {
    //     res.status(500).send({ message: "Error de servidor" });
    // }

}

getAvatar = async (req, res) => {
    const avatarName = req.params.avatarName;

    const filePath = `./uploads/avatar/${avatarName}`;

    try {
        fs.access(filePath, fs.constants.F_OK, (err) => {
            if (err) {
                // console.log(exists);
                res.status(404).send({ message: "Ela avatar no existe" });
            } else {
                res.sendFile(path.resolve(filePath));
            }
        })
    } catch (error) {
        res.status(500).send({ message: "Ha ocurrido un error en el servidor" });
    }


}

updateUser = async (req, res) => {
    const userData = req.body;
    userData.email = req.body.email.toLowerCase();
    const params = req.params;

    if (userData.password) {
        await bcrypt.hash(userData.password, null, null, (err, hash) => {
            if (err) {
                res.status(500).send({ message: "Error al encriptar la contraseña" });
            } else {
                userData.password = hash;
            }
        })
    }

    try {
        let user = await User.findByIdAndUpdate({ _id: params.id }, userData);
        if (!user) {
            res.status(404).send({ message: "No se ha encontrado el usuario" });
        } else {
            res.status(200).send({ message: "Usuario Actualizado" });
        }
    } catch (error) {
        res.status(500).send({ message: "Ha ocurrido un error en el servidor" });
    }


    // console.log(userData);
}

activateUser = async (req, res) => {
    const { id } = req.params;
    const { active } = req.body;

    try {
        User.findByIdAndUpdate(id, { active }, (err, userStored) => {


            if (err) {
                res.status(500).send({ message: "Error al de servidor" });
            } else {
                if (!userStored) {
                    res.status(404).send({ message: "El usuario no existe" });
                } else {
                    if (active) {
                        res.status(200).send({ message: "Usuario Activado Correctamente" });
                    } else {
                        res.status(200).send({ message: "Usuario Desactivado Correctamente" });
                    }

                }
            }
        });
    } catch (error) {
        res.status(500).send({ message: "Error al de servidor" });
    }



}

deleteUser = async (req, res) => {
    const { id } = req.params;

    User.findByIdAndRemove(id, (err, userDeleted) => {
        if (err) {
            res.status(500).send({ message: "Error del servidor" });
        } else {
            if (!userDeleted) {
                res.status(404).send({ message: "Usuario no existe" });
            } else {
                res.status(200).send({ message: "Usuario Eliminado" });
            }
        }
    })
}

signUpAdmin = async (req, res) => {

    const user = new User();

    const { name, lastname, email, role, password } = req.body
    user.name = name;
    user.lastname = lastname;
    user.email = email.toLowerCase();
    user.role = role;
    user.active = true;

    if (!password) {
        res.status(500).send({ message: "la contraseña es obligatoria" });
    } else {
        bcrypt.hash(password, null, null, (err, hash) => {
            if (err) {
                res.status(500).send({ message: "Error encriptando la contraseña" });
            } else {
                user.password = hash;

                user.save((err, userStored) => {
                    if (err) {
                        res.status(500).send({ message: "Error Creando el usuario, verifique que no exita." });
                    } else {
                        if (!userStored) {
                            res.status(500).send({ message: "Error creando el usuario" });
                        } else {
                            res.status(200).send({ user: userStored });
                        }
                    }
                })
            }
        })
    }


}

module.exports = {
    signUp,
    signIn,
    getUsers,
    getUsersActive,
    uploadAvatar,
    getAvatar,
    updateUser,
    activateUser,
    deleteUser,
    signUpAdmin
}
