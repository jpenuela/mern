const mongoose = require("mongoose");
//const app = require("./app");
const PORT_SERVER = process.env.PORT || 8080;
const { API_VERSION, PORT_DB, IP_SERVER, USARIO_MONGO, PASSWORD_MONGO, DB } = require("./../config");

// mongoose.set('useFindAndModify', false);

mongoose.connect(`mongodb://${USARIO_MONGO}:${PASSWORD_MONGO}@${IP_SERVER}:${PORT_DB}/${DB}`,
    { useNewUrlParser: true, useUnifiedTopology: true }, (err, res) => {
        if (err) {
            throw err;
        } else {
            console.log("La conexion a la base es correcta");
            //app.listen(PORT_SERVER, () => {
            console.log("#####################");
            console.log("##### API REST ######");
            console.log("#####################");
            console.log(`http:/'/${IP_SERVER}:${PORT_SERVER}/api/${API_VERSION}/`);
            //})
        }
    });

module.exports = true;




