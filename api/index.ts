const express = require("express");
const https = require("https");
//import fs from 'fs';
const fs = require('fs')
//import path from 'path';
const path = require('path')
const bodyParser = require("body-parser");

const { API_VERSION } = require("./config")

const authRoutes = require("./routers/auth");
const userRoutes = require("./routers/user");
const tiedaRoutes = require("./routers/tienda");
const menuRoutes = require("./routers/menu");

const x = require('./db/mongoose')
const cors = require('cors');
const app = express();
const port = 8080;
const securePort = 8443;
// const allowedOrigins = ['*'];
let jsonParser = bodyParser({ limit: '50mb' })
const creds = {
  cert: fs.readFileSync(path.resolve(__dirname, "./certs/cert.pem"), 'utf8'),
  key: fs.readFileSync(path.resolve(__dirname, "./certs/key.pem"), 'utf8')
}
https.createServer(creds, app).listen(securePort, function () {
  console.log(`backend listening at https://localhost:${securePort}`);
});

app.use(bodyParser.urlencoded({ extends: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use(jsonParser)
app.use(cors());
app.use(`/api/${API_VERSION}`, authRoutes);
app.use(`/api/${API_VERSION}`, userRoutes);
app.use(`/api/${API_VERSION}`, tiedaRoutes);
app.use(`/api/${API_VERSION}`, menuRoutes);

app.get("/", (req, res) => {
  res.send("Hello from backend");
});

app.post("/", (req, res) => {
  res.json(req.body);
});

app.listen(port, () => {
  console.log(`backend listening at http://localhost:${port}`);
});


module.exports = app;