const express = require('express');
const TiendaController = require('../controllers/tienda');

const md_auth = require('../middleware/authenticated');

const api = express.Router();

api.get("/tiendas", TiendaController.getTiendas);
api.get("/tiendas-active", [md_auth.ensureAuth], TiendaController.getTiendasActive);
api.put("/update-user/:id", [md_auth.ensureAuth], TiendaController.updateTienda);
api.put("/activate-user/:id", [md_auth.ensureAuth], TiendaController.activateTienda);
api.delete("/delete-user/:id", [md_auth.ensureAuth], TiendaController.deleteTienda);
api.post("/new-tienda", TiendaController.newTienda);
api.post("/tienda-service", [md_auth.ensureAuth], TiendaController.tiendaService);

module.exports = api;