const jwt = require('jwt-simple');
const moment = require('moment');

const SCRET_KEY = "4d6ddde01eeca961dce9062f271d1e73";


exports.createAccessToken = function (user) {
    const payload = {
        id: user._id,
        name: user.name,
        lastname: user.lastname,
        email: user.email,
        role: user.role,
        createToken: moment().unix(),
        exp: moment().add(2, 'minutes').unix()
    };

    return jwt.encode(payload, SCRET_KEY);
};

exports.createRefreshToken = function (user) {
    const payload = {
        id: user._id,
        exp: moment().add(30, "days").unix(),
    }

    return jwt.encode(payload, SCRET_KEY);
}

exports.decodedToken = function (token) {
    return jwt.decode(token, SCRET_KEY);
}
