import { basePath, apiVersion } from "./config";


export function getMenuApi() {

    const url = `${basePath}/${apiVersion}/menu`;
    // const params = {
    //     method: "GET",
    //     headers: {
    //         "Content-Type": "application/json",
    //     },
    // };
    return fetch(url)
        .then((response) => {
            return response.json();
        })
        .then((result) => {
            return result;
        })
        .catch((err) => {
            return err.message;
        });

}