import React from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  PoweroffOutlined,
} from "@ant-design/icons";
import SumasLogo from "../../../assets/img/png/sumas_left.png";
import { logout } from "../../../api/auth";
import { Redirect } from "react-router";

import "./MenuTop.scss";

export default function MenuTop(props) {
  // console.log(props);
  const { menuCollapsed, setMenuCollapsed } = props;
  const MenuFold = menuCollapsed ? MenuUnfoldOutlined : MenuFoldOutlined;

  const logoutUser = () => {
    logout();
    window.location.reload();
  };
  return (
    <div className="menu-top">
      <div className="menu-top__left">
        <Link to={"/admin"}>
          <img
            className="menu-top__left-logo"
            src={SumasLogo}
            alt="Juan Pablo Penuela "
          />
        </Link>
        <Button type="link" onClick={() => setMenuCollapsed(!menuCollapsed)}>
          <MenuFold />
        </Button>
      </div>
      <div className="menu-top__right">
        <Button type="link" onClick={() => logoutUser()}>
          <PoweroffOutlined />
        </Button>
      </div>
    </div>
  );
}
