import React, { useState, useEffect } from 'react'
import { Switch, List, Button, Modal as ModalAntd, notification } from "antd";
import { } from "@ant-design/icons";
import Modal from "../../../Modal/Modal"
import DragSortableList from "react-drag-sortable"

import "./MenuWebList.scss"

const { confirm } = ModalAntd

export default function MenuwebList(props) {

    const { menu, setReloadMenu } = props;

    useEffect(() => {
        const listItemsArray = [];
        menu.forEach(item => {
            listItemsArray.push({
                // content: (<div><p>{item.title}</p><div />)
            })
        });
    }, [menu])

    const onSort = (sortedList, dropEvent) => {
        console.log(sortedList);
    }

    return (

        <div className='menu-web-list'>
            <div className='menu-web-list__header'>
                <Button>Menu menu</Button>
            </div>

            <div className='menu-web-list__items'>
                <DragSortableList />
            </div>
        </div>

    )


}
