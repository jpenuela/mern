import React, { useState, useEffect } from "react";
import { Switch, List, Avatar, Button, notification, Modal as ModalAntd } from "antd";
import {
  EditOutlined,
  StopOutlined,
  DeleteOutlined,
  CheckOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import NoAvatar from "../../../../assets/img/png/no-avatar.png";
import Modal from "../../../Modal";
import EditUserForm from "../EditUserForm";
import AddUserForm from "../AddUserForm";
import { getAvatarApi, activateUserApi, deleteUserApi } from "../../../../api/user";
import { getAccesTokenApi } from "../../../../api/auth";

import "./ListUsers.scss";

const { confirm } = ModalAntd;

export default function ListUsers(props) {
  const { usersActive, usersInActive, setReloadUsers } = props;
  const [viewUsersActives, setViewUsersActives] = useState(true);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalContent, setModalContent] = useState(null);

  const showDeleteConfirm = (user) => {
    const token = getAccesTokenApi();
    confirm({
      title: "Eliminando Usuario...",
      content: `Esta seguro que va a eliminar el usuario ${user.email}`,
      okText: "Si, Seguro",
      okType: "danger",
      cancelText: "Cncelar",
      onOk() {
        deleteUserApi(token, user._id).then(response => {
          notification["success"]({ message: response });
          setReloadUsers(true)
        }).catch(err => {
          notification["error"]({ message: err })
        })
      }
    })
  }

  const addUserModal = async () => {
    setIsVisibleModal(true);
    setModalTitle("Agregar Nuevo Ususario")
    setModalContent(
      <AddUserForm setIsVisibleModal={setIsVisibleModal} setReloadUsers={setReloadUsers} />
    )
  }

  return (
    <div className="list-users">

      <div className="list-users__header">
        <div className="list-users__header-switch">
          <Switch
            defaultChecked
            onChange={() => setViewUsersActives(!viewUsersActives)}
          />
          <span>
            {viewUsersActives ? "Usuario Activos" : "Usuarios Inactivos"}
          </span>
        </div>
        <Button type="primary" onClick={addUserModal}>
          <UserAddOutlined />
        </Button>
      </div>


      {viewUsersActives ? (
        <UsersActive
          usersActive={usersActive}
          setIsVisibleModal={setIsVisibleModal}
          setModalTitle={setModalTitle}
          setModalContent={setModalContent}
          setReloadUsers={setReloadUsers}
          showDeleteConfirm={showDeleteConfirm}

        />
      ) : (
        <UsersInActive
          usersInActive={usersInActive}
          setReloadUsers={setReloadUsers}
          showDeleteConfirm={showDeleteConfirm}
        />
      )}
      <Modal
        title={modalTitle}
        isVisible={isVisibleModal}
        setIsVisible={setIsVisibleModal}
      >
        {modalContent}
      </Modal>
    </div>
  );
}

function UsersActive(props) {
  const { usersActive, setIsVisibleModal, setModalTitle, setModalContent, setReloadUsers, showDeleteConfirm } =
    props;
  const editUser = (usr) => {
    setIsVisibleModal(true);
    setModalTitle(
      `Editar  ${usr.name ? usr.name : "..."}  ${usr.lastname ? usr.lastname : "..."
      }`
    );
    setModalContent(<EditUserForm user={usr} setIsVisibleModal={setIsVisibleModal} setReloadUsers={setReloadUsers} />);
  };
  return (
    <List
      className="users-active"
      itemLayout="horizontal"
      dataSource={usersActive}
      renderItem={(user) => <UserActive user={user} editUser={editUser} setReloadUsers={setReloadUsers} showDeleteConfirm={showDeleteConfirm} />}
    />
  );
}

function UserActive(props) {
  const { user, editUser, setReloadUsers, showDeleteConfirm } = props;

  const [avatar, setAvatar] = useState(null);

  useEffect(() => {
    if (user.avatar) {
      getAvatarApi(user.avatar).then(response => {
        setAvatar(response);
      })
    } else {
      setAvatar(null);
    }
  }, [user]);

  const desactivateUSer = () => {
    const token = getAccesTokenApi()

    activateUserApi(token, false, user._id).then(response => {
      notification["success"]({ message: response });
      setReloadUsers(true);
    }).catch(err => {
      notification["error"]({ message: err })
    })
  }



  return (
    <List.Item
      actions={[
        <Button type="primary" onClick={() => editUser(user)}>
          <EditOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={desactivateUSer}
        >
          <StopOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={e => showDeleteConfirm(user)}
        >
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta
        avatar={<Avatar src={avatar ? avatar.url : NoAvatar} />}
        title={`
                ${user.name ? user.name : "..."} 
                ${user.lastname ? user.lastname : "..."}
            `}
        description={user.email}
      />
    </List.Item>
  )

}

function UsersInActive(props) {
  const { usersInActive, setReloadUsers, showDeleteConfirm } = props;
  return (
    <List
      className="users-active"
      itemLayout="horizontal"
      dataSource={usersInActive}
      renderItem={(user) => <UserInactive user={user} setReloadUsers={setReloadUsers} showDeleteConfirm={showDeleteConfirm} />}
    />
  );
}

function UserInactive(props) {
  const { user, setReloadUsers, showDeleteConfirm } = props;

  const [avatar, setAvatar] = useState(null);

  useEffect(() => {
    if (user.avatar) {
      getAvatarApi(user.avatar).then(response => {
        setAvatar(response);
      })
    } else {
      setAvatar(null);
    }
  }, [user]);

  const activateUSer = () => {
    const token = getAccesTokenApi()

    activateUserApi(token, true, user._id).then(response => {
      notification["success"]({ message: response });
      setReloadUsers(true);
    }).catch(err => {
      notification["error"]({ message: err })
    })
  }



  return (
    <List.Item
      actions={[
        <Button
          type="primary"
          onClick={activateUSer}
        >
          <CheckOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={e => showDeleteConfirm(user)}
        >
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta
        avatar={<Avatar src={avatar ? avatar.url : NoAvatar} />}
        title={`
                ${user.name ? user.name : "..."} 
                ${user.lastname ? user.lastname : "..."}
            `}
        description={user.email}
      />
    </List.Item>
  )
}

