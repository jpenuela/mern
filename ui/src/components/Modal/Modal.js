import React from "react";
import { Modal as ModalAnd } from "antd";

export default function Modal(props) {
  const { children, title, isVisible, setIsVisible } = props;

  return (
    <ModalAnd
      title={title}
      centered
      visible={isVisible}
      onCancel={() => setIsVisible(false)}
      footer={false}
    >
      {children}
    </ModalAnd>
  );
}
