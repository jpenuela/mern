import { useContext } from "react";
import { AuthContext } from "../Providers/AuthProviders";

export default () => useContext(AuthContext); 
