import React, { useState, useEffect } from "react";
import "./Users.scss";
import { getAccesTokenApi } from "../../../api/auth";
import { getUsersActiveApi } from "../../../api/user";
import ListUsers from "../../../components/Admin/Users/ListUsers";

export default function Users() {
  const [usersActive, setUsersActive] = useState([]);
  const [usersInActive, setUsersInActive] = useState([]);
  const [reloadUsers, setReloadUsers] = useState(false);
  const token = getAccesTokenApi();

  useEffect(() => {
    getUsersActiveApi(token, true).then((response) => {
      setUsersActive(response.users);
      // console.log(response.users);
    });
    getUsersActiveApi(token, false).then((response) => {
      setUsersInActive(response.users);
      // console.log(response.users);
    });
    setReloadUsers(false);
  }, [token, reloadUsers]);



  return (
    <div className="users">
      <ListUsers usersActive={usersActive} usersInActive={usersInActive} setReloadUsers={setReloadUsers} />
    </div>
  );
} 
